export default[
  {
    "Id": 1,
    "Name": "Online Tic Tac Toe Game",
    "Technology": [{ "id":1,"Tech" :"ReactJS" }, { "id":2,"Tech" : "HTML5" },{ "id":3,"Tech" :"CSS3" }, { "id":4,"Tech" : "Socket.io" }  ],
    "Gitlink": "#",
    "Type": "Web Development",
    "Link": "https://harshit-18mcmc42.herokuapp.com/"
  },
  {
    "Id": 2,
    "Name": "Nielit Solutions Website",
    "Technology": [{ "id":1,"Tech" :"ReactJS" }, { "id":2,"Tech" : "ExpressJS" }, {"id":3, "Tech": "Firebase"} ],
    "Gitlink": "https://Harshit-18mcmc42@bitbucket.org/Harshit-18mcmc42/nielit-solutions.git",
    "Type": "Web Development",
    "Link": "https://nielitsolutions.com"
  },
  {
    "Id": 3,
    "Name": "Portfolio Website",
    "Technology": [{ "id":1,"Tech" :"ReactJS" }, { "id":2,"Tech" : "ExpressJS" }, {"id":3, "Tech": "Firebase"} ],
    "Gitlink": "https://Harshit-18mcmc42@bitbucket.org/Harshit-18mcmc42/harshit-portfolio.git",
    "Type": "Web Development",
    "Link": "#"
  },
  {
    "Id": 4,
    "Name": "Simple-Todos",
    "Technology": [{ "id":1,"Tech" :"React" }, { "id": 2,"Tech" : "Redux" }, {"id" : 3, "Tech" : "Firebase"} ],
    "Gitlink": "https://Harshit-18mcmc42@bitbucket.org/Harshit-18mcmc42/simple-todos.git",
    "Type": "Web Development",
    "Link": "https://simple-todos-babdb.web.app/"
  },
  {
    "Id": 5,
    "Name": "AI Game 'Stay Home'",
    "Technology": [{ "id":1,"Tech" :"Java" }, { "id":2,"Tech" : "Java Swing" }, {"id": 3, "Tech": "Derby DB"} ],
    "Gitlink": "https://Harshit-18mcmc42@bitbucket.org/Harshit-18mcmc42/stay-home.git",
    "Type": "Application Software",
    "Link": "https://drive.google.com/file/d/1ITg0oxU0A5a6iF_JATEei55XqjRoUx19/view"
  },
  {
    "Id": 6,
    "Name": "MERN Website",
    "Technology": [{ "id":1,"Tech" :"Mongodb" }, { "id":2,"Tech" : "ExpressJS" },{ "id":3,"Tech" :"ReactJS" }, { "id":4,"Tech" : "NodeJS" }  ],
    "Gitlink": "https://Harshit-18mcmc42@bitbucket.org/Harshit-18mcmc42/mern-stack.git",
    "Type": "Web Development",
    "Link": "#"
  }
]
